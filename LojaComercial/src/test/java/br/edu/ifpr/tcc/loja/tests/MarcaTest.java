/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.tests;

import br.edu.ifpr.tcc.loja.controle.MarcaControle;
import br.edu.ifpr.tcc.loja.entidade.Marca;
import br.edu.ifpr.tcc.loja.facade.MarcaFacade;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class MarcaTest {

    @Test
    public void cadastraMarca() {
        Marca marca = new Marca();
        MarcaFacade f = new MarcaFacade();
        marca.setId(Long.getLong("10"));
        marca.setDescricaoMarca("marca teste");
        try {
            f.salvar(marca);
        } catch (Exception e) {
            e.printStackTrace();
        }   
        Assert.assertEquals("marca teste", marca.getDescricaoMarca());
    }
}

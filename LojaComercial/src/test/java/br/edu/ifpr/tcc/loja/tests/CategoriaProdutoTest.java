/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.tests;

import br.edu.ifpr.tcc.loja.controle.CategoriaProdutoControle;
import br.edu.ifpr.tcc.loja.entidade.CategoriaProduto;
import br.edu.ifpr.tcc.loja.facade.CategoriaProdutoFacade;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class CategoriaProdutoTest {

    @Test
    public void cadastraCategoriaProduto() {
        CategoriaProduto categoriaProduto = new CategoriaProduto();
        CategoriaProdutoFacade f = new CategoriaProdutoFacade();
        categoriaProduto.setId(Long.getLong("10"));
        categoriaProduto.setDescricaoCategoriaProduto("categoriaProduto teste");
        try {
            f.salvar(categoriaProduto);
        } catch (Exception e) {
            e.printStackTrace();
        }  
        Assert.assertEquals("categoriaProduto teste", categoriaProduto.getDescricaoCategoriaProduto());
    }
}

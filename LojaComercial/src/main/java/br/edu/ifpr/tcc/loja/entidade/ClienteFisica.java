/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "cliente_fisica")
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "pessoa_id", referencedColumnName = "pessoa_id", foreignKey = @ForeignKey(name = "FK_CLIENTE_FISICA_PESSOA"))
@AttributeOverrides({@AttributeOverride(name = "pessoa_id", column = @Column(name = "pessoa_id"))})
public class ClienteFisica extends Pessoa implements Serializable {

//    @Column(name = "cliente_nome", nullable = false)
//    private String nome;
    @Column(name = "cliente_cpf", nullable = true)
    private String cpf;
    @Column(name = "cliente_rg", nullable = true)
    private String rg;
    @Column(name = "cliente_dt_nascimento", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataNascimento;

//    public String getNome() {
//        return nome;
//    }
//
//    public void setNome(String nome) {
//        this.nome = nome;
//    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String getDocumentoFederal() {
        return cpf;
    }

    @Override
    public String getDocumentoEstadual() {
        return rg;
    }

    @Override
    public void setDocumentoFederal(String doc) {
        this.cpf = doc;
    }
    
}

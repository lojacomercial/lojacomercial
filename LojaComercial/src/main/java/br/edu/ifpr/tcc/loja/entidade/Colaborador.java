/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "colaborador")
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "pessoa_id", referencedColumnName = "pessoa_id", foreignKey = @ForeignKey(name = "FK_FORNECEDOR_PESSOA"))
@AttributeOverrides({@AttributeOverride(name = "pessoa_id", column = @Column(name = "colaborador_id"))})
public class Colaborador extends ClienteFisica implements Serializable {

    @Column(name = "colaborador_pis", nullable = false)
    private String pis;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id", nullable = false, foreignKey = @ForeignKey(name = "FK_COLABORADOR_USUARIO"))
    private Usuario usuario; 

    public String getPis() {
        return pis;
    }

    public void setPis(String pis) {
        this.pis = pis;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }        
    
}

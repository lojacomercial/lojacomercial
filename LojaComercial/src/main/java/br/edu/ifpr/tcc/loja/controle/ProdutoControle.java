/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.entidade.Produto;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.facade.ProdutoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Named
@ViewScoped
public class ProdutoControle extends AbstractControle<Produto> 
                             implements Serializable{

    @Inject
    private ProdutoFacade produtoFacade;
    
    public ProdutoControle() {
        super(Produto.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return produtoFacade;
    }
    
    public List<Produto> produtoLikeDescricao(String query){
        return produtoFacade.produtoLikeDescricao(query);
    }
  
}

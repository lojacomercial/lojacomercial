/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.facade;

import br.edu.ifpr.tcc.loja.entidade.Produto;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class ProdutoFacade extends AbstractFacade<Produto> implements Serializable{

    @Inject
    private EntityManager em;
    
    public ProdutoFacade() {
        super(Produto.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<Produto> produtoLikeDescricao(String query){
        Query q = em.createQuery("FROM Produto AS p "
           + " WHERE LOWER(p.descricaoProduto) LIKE ('%"+query.toLowerCase()+"%') "
           + " OR LOWER(to_char(p.id,'999')) LIKE ('%"+query.toLowerCase()+"%') "
           + " ORDER BY p.descricaoProduto");
        return q.setMaxResults(20).getResultList();
    }
    
}

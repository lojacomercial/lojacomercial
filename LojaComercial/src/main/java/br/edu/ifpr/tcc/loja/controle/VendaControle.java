/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.entidade.Venda;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.facade.VendaFacade;
import br.edu.ifpr.tcc.loja.util.ControleUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@ViewScoped
public class VendaControle extends AbstractControle<Venda>
        implements Serializable {

    @Inject
    private VendaFacade vendaFacade;

    public VendaControle() {
        super(Venda.class);
    }

    public void relacionaPreco() {
        BigDecimal preco = super.getEntidade().getVendaItem().getProduto().getValorVenda();
        super.getEntidade().getVendaItem().setPreco(preco);
    }

    @Override
    public String salvar() {
        try {
            super.setEntidade(vendaFacade.salvar(super.getEntidade()));
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", "");
            return "list?faces-redirect=true";
        } catch (Exception e) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_FATAL, "Erro ao salvar venda", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AbstractFacade getFacade() {
        return vendaFacade;
    }

    @Override
    public void alterar() {
        vendaFacade.carregaItenVenda(super.getEntidade());
        super.alterar();
    }

}

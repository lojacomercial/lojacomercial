/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.facade;

import br.edu.ifpr.tcc.loja.entidade.Marca;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class MarcaFacade extends AbstractFacade<Marca> implements Serializable{

    @Inject
    private EntityManager em;
    
    public MarcaFacade() {
        super(Marca.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<Marca> marcaLikeDescricao(String query){
        Query q = em.createQuery("FROM marca AS m "
           + "WHERE LOWER(m.descricaoMarca) LIKE ('%"+query.toLowerCase()+"%') OR "
           + "LOWER(m.marca_id) LIKE ('%"+query.toLowerCase()+"%') "
           + "ORDER BY m.descricaoMarca");
        return q.setMaxResults(20).getResultList();
    }
    
}

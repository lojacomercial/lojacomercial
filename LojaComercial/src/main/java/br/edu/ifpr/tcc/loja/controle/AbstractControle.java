/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.converter.ConverterGenerico;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.util.ControleUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;

/**
 *
 * @author Rodolfo Cassorillo
 * @param <T>
 */
public abstract class AbstractControle<T> implements Serializable {

    private Class<T> classe;
    private List<T> listagem;
    private T entidade;
    private Boolean layoutList = true;
    private Boolean layoutForm = false;
    private ConverterGenerico converter;

    public ConverterGenerico converter() {
        if (converter == null) {
            converter = new ConverterGenerico(getFacade());
        }
        return converter;
    }

    public AbstractControle(Class<T> classe) {
        this.classe = classe;
    }

    public abstract AbstractFacade<T> getFacade();

    public void novo() {
        try {
            entidade = classe.newInstance();
            layoutList = false;
            layoutForm = true;
        } catch (InstantiationException | IllegalAccessException ex) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_FATAL, "Erro ao instanciar", ex.getMessage());
        }
    }

    public void alterar() {
        layoutList = false;
        layoutForm = true;
    }

    public String salvar() {
        try {
            getFacade().salvar(entidade);
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Salvo com sucesso", "");
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            ex.printStackTrace();
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_FATAL, "Erro ao salvar " + classe.getSimpleName(), ex.getMessage());
        }
        return null;
    }

    public String excluir() {
        try {
            getFacade().excluir(entidade);
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Excluido com sucesso", "");
            return "list?faces-redirect=true";
        } catch (Exception ex) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_FATAL, "Erro ao excluir " + classe.getSimpleName(), ex.getMessage());
        }
        return null;
    }

    public List<T> getListar() {
        if (listagem == null) {
            listagem = getFacade().listar();
        }
        return listagem;
    }

    public T getEntidade() {
        return entidade;
    }

    public void setEntidade(T entidade) {
        this.entidade = entidade;
    }

    public Boolean getLayoutList() {
        return layoutList;
    }

    public void setLayoutList(Boolean layoutList) {
        this.layoutList = layoutList;
    }

    public Boolean getLayoutForm() {
        return layoutForm;
    }

    public void setLayoutForm(Boolean layoutForm) {
        this.layoutForm = layoutForm;
    }

}

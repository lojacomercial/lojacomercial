/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "cliente_juridico")
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "pessoa_id", referencedColumnName = "pessoa_id", foreignKey = @ForeignKey(name = "FK_CLIENTE_JURIDICO_PESSOA"))
@AttributeOverrides({@AttributeOverride(name = "pessoa_id", column = @Column(name = "pessoa_id"))})
public class ClienteJuridico extends Pessoa implements Serializable {

//    @Column(name = "cliente_razao_social", nullable = false)
//    private String razaoSocial;
    @Column(name = "cliente_cnpj", nullable = true)
    private String cnpj;
    @Column(name = "cliente_ie", nullable = true)
    private String inscricao;
    @Column(name = "cliente_dt_fundacao", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataFundacao;

//    public String getRazaoSocial() {
//        return razaoSocial;
//    }
//
//    public void setRazaoSocial(String razaoSocial) {
//        this.razaoSocial = razaoSocial;
//    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public Date getDataFundacao() {
        return dataFundacao;
    }

    public void setDataFundacao(Date dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    @Override
    public String getDocumentoFederal() {
        return cnpj;
    }

    @Override
    public String getDocumentoEstadual() {
        return inscricao;
    }

    @Override
    public void setDocumentoFederal(String doc) {
        this.cnpj = doc;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.facade;

import br.edu.ifpr.tcc.loja.entidade.CategoriaProduto;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class CategoriaProdutoFacade extends AbstractFacade<CategoriaProduto> implements Serializable{

    @Inject
    private EntityManager em;
    
    public CategoriaProdutoFacade() {
        super(CategoriaProduto.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public List<CategoriaProduto> categoriaProdutoLikeDescricao(String query){
        Query q = em.createQuery("FROM categoria_produto AS cp "
           + " WHERE LOWER(cp.categoria_produto_descricao) LIKE ('%"+query.toLowerCase()+"%') OR "
           + " LOWER(cp.categoria_produto_id) LIKE ('%"+query.toLowerCase()+"%') "
           + " ORDER BY cp.categoria_produto_descricao ");
        return q.setMaxResults(20).getResultList();
    }
    
}

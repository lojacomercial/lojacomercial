/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.facade;


import br.edu.ifpr.tcc.loja.entidade.ClienteFisica;
import br.edu.ifpr.tcc.loja.entidade.ClienteJuridico;
import br.edu.ifpr.tcc.loja.entidade.Pessoa;
import java.io.Serializable;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */

public class PessoaFacade extends AbstractFacade<Pessoa> implements Serializable {
    
    @Inject
    private EntityManager em;

    public PessoaFacade() {
        super(Pessoa.class);
    }

    @Override
    public EntityManager getEm() {
        return em;
    }
    
    public ClienteJuridico buscaPorCnpj(String cnpj) throws Exception{
        try{
           ClienteJuridico pj;
           
           pj = (ClienteJuridico) 
           em.createQuery("FROM cliente_juridico AS pj WHERE pj.cnpj='"+cnpj+"'")
           .getSingleResult();
           
           return pj;
        }catch(Exception ex){
            throw ex;
        }
    }
    
    
    public ClienteFisica buscaPorCpf(String cpf) throws Exception{
        try{
           ClienteFisica pf;
           
           pf = (ClienteFisica) 
           em.createQuery("FROM cliente_fisica AS pf WHERE pf.cpf='"+cpf+"'")
           .getSingleResult();
           
           return pf;
        }catch(Exception ex){
            throw ex;
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "produto")
//@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PRODUTO")
    @SequenceGenerator(name = "SEQ_PRODUTO", sequenceName = "SEQ_PRODUTO")
    @Column(name = "produto_id")
    private Long id;
    @Column(name = "produto_descricao", nullable = false)
    private String descricaoProduto;
    @Column(name = "produto_ncm")
    private String ncm;
    @Column(name = "produto_codigo_barra")
    private String codigoBarra;
    @Column(name = "produto_valor_custo", nullable = false)
    private BigDecimal valorCusto;
    @Column(name = "produto_valor_venda", nullable = false)
    private BigDecimal valorVenda;
    @Column(name = "produto_qt_estoque", nullable = false)
    private BigDecimal quantidadeEstoque;
    @Column(name = "produto_qt_min_estoque", nullable = false)
    private BigDecimal quantidadeMinEstoque;
    @Column(name = "produto_qt_max_estoque", nullable = false)
    private BigDecimal quantidadeMaxEstoque;
    @Column(name = "produto_qt_multiplo")
    private BigDecimal quantidadeMultiplo;
    @Column(name = "produto_ativo")
    private Boolean ativo = true;
    @Column(name = "produto_foto")
    private Blob foto;
    @Column(name = "produto_pc_max_desconto")
    private BigDecimal pcMaxDesconto;
    @ManyToOne
    @JoinColumn(name = "categoria_produto_id", referencedColumnName = "categoria_produto_id", nullable = true, foreignKey = @ForeignKey(name = "FK_PRODUTO_CATEGORIA"))
    private CategoriaProduto categoriaProduto;
    @ManyToOne
    @JoinColumn(name = "marca_id", referencedColumnName = "marca_id", nullable = true, foreignKey = @ForeignKey(name = "FK_PRODUTO_MARCA"))
    private Marca marca;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public String getNcm() {
        return ncm;
    }

    public void setNcm(String ncm) {
        this.ncm = ncm;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public BigDecimal getValorCusto() {
        return valorCusto;
    }

    public void setValorCusto(BigDecimal valorCusto) {
        this.valorCusto = valorCusto;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    public BigDecimal getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(BigDecimal quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public BigDecimal getQuantidadeMultiplo() {
        return quantidadeMultiplo;
    }

    public void setQuantidadeMultiplo(BigDecimal quantidadeMultiplo) {
        this.quantidadeMultiplo = quantidadeMultiplo;
    }    

    public BigDecimal getQuantidadeMinEstoque() {
        return quantidadeMinEstoque;
    }

    public void setQuantidadeMinEstoque(BigDecimal quantidadeMinEstoque) {
        this.quantidadeMinEstoque = quantidadeMinEstoque;
    }

    public BigDecimal getQuantidadeMaxEstoque() {
        return quantidadeMaxEstoque;
    }

    public void setQuantidadeMaxEstoque(BigDecimal quantidadeMaxEstoque) {
        this.quantidadeMaxEstoque = quantidadeMaxEstoque;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Blob getFoto() {
        return foto;
    }

    public void setFoto(Blob foto) {
        this.foto = foto;
    }

    public BigDecimal getPcMaxDesconto() {
        return pcMaxDesconto;
    }

    public void setPcMaxDesconto(BigDecimal pcMaxDesconto) {
        this.pcMaxDesconto = pcMaxDesconto;
    }

    public CategoriaProduto getCategoriaProduto() {
        return categoriaProduto;
    }

    public void setCategoriaProduto(CategoriaProduto categoriaProduto) {
        this.categoriaProduto = categoriaProduto;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id.toString();
    }

}

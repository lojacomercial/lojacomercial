/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author Rodolfo Cassorillo
 */
@MappedSuperclass
public abstract class AbstractClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "data_cadastro")
    private Date dataCadastro;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "observacao")
    private String observacao;
    
    public AbstractClass() {}

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    } 
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        System.out.println(obj.getClass().getSimpleName());
        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "pessoa")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_PESSOA")
    @SequenceGenerator(name = "SEQ_PESSOA", sequenceName = "SEQ_PESSOA")
    @Column(name = "pessoa_id")
    private Long id;
    @Column(name = "pessoa_email", nullable = true)
    private String email;
    @Column(name = "pessoa_nome_razao", nullable = true)
    private String nome_razao;
    @Column(name = "pessoa_logradouro", nullable = true)
    private String logradouro;
    @Column(name = "pessoa_numero", nullable = true)
    private String numero;
    @Column(name = "pessoa_bairro", nullable = true)
    private String bairro;
    @Column(name = "pessoa_cep", nullable = true)
    private String cep;
    @Column(name = "pessoa_complemento", nullable = true)
    private String complemento;
    @Column(name = "pessoa_telefone_fixo", nullable = true)
    private String telefoneFixo;
    @Column(name = "pessoa_telefone_celular", nullable = true)
    private String telefoneCelular;
    @Column(name = "pessoa_bo_cliente")
    private Boolean boCliente;
    @Column(name = "pessoa_bo_fornecedor")
    private Boolean boFornecedor;
    @Column(name = "pessoa_bo_colaborador")
    private Boolean boColaborador;
    
    public abstract String getDocumentoFederal();

    public abstract void setDocumentoFederal(String doc);

    public abstract String getDocumentoEstadual();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome_razao() {
        return nome_razao;
    }

    public void setNome_razao(String nome_razao) {
        this.nome_razao = nome_razao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public Boolean getBoCliente() {
        return boCliente;
    }

    public void setBoCliente(Boolean boCliente) {
        this.boCliente = boCliente;
    }

    public Boolean getBoFornecedor() {
        return boFornecedor;
    }

    public void setBoFornecedor(Boolean boFornecedor) {
        this.boFornecedor = boFornecedor;
    }

    public Boolean getBoColaborador() {
        return boColaborador;
    }

    public void setBoColaborador(Boolean boColaborador) {
        this.boColaborador = boColaborador;
    }    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id.toString();
    }
    
}

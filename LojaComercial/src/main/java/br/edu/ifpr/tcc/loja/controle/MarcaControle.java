/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.entidade.Marca;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.facade.MarcaFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Named
@ViewScoped
public class MarcaControle extends AbstractControle<Marca> implements Serializable{

    @Inject
    private MarcaFacade marcaFacade;
    
    public MarcaControle() {
        super(Marca.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return marcaFacade;
    }
    
    public List<Marca> marcaLikeDescricao(String query){
        return marcaFacade.marcaLikeDescricao(query);
    }
  
}

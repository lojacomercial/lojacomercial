/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

/**
 *
 * @author Rodolfo Cassorillo
 */
public enum Genero {

    MASCULINO(1), 
    FEMININO(2);

    public int genero;

    private Genero(int genero) {
        this.genero = genero;
    }

}

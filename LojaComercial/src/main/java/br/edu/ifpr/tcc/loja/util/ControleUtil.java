/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Rodolfo Cassorillo
 */
public class ControleUtil {
    
     public static void mensagemSistema(FacesMessage.Severity tipo, String msg, String detalhe) {
        FacesMessage message = new FacesMessage(tipo, msg, detalhe);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
}

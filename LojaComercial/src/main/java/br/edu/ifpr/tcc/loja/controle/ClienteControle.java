/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.entidade.Cliente;
import br.edu.ifpr.tcc.loja.entidade.ClienteFisica;
import br.edu.ifpr.tcc.loja.entidade.ClienteJuridico;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.facade.ClienteFacade;
import br.edu.ifpr.tcc.loja.facade.PessoaFacade;
import br.edu.ifpr.tcc.loja.util.ControleUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 */
@Named
@ViewScoped
public class ClienteControle extends AbstractControle<Cliente> implements Serializable {

    @Inject
    private ClienteFacade clienteFacade;
    private String tipoPessoa = "PF";
    private String documentoFederal;
    @Inject
    private PessoaFacade pessoaFacade;

    @Override
    public void novo() {
        super.setEntidade(new Cliente());
        selecionaPessoa();
        super.setLayoutList(false);
        super.setLayoutForm(true);
    }

    @Override
    public void alterar() {
        super.alterar();
        if (super.getEntidade().getPessoa() instanceof ClienteJuridico) {
            tipoPessoa = "PJ";
        } else {
            tipoPessoa = "PF";
        }
        documentoFederal = getEntidade().getPessoa().getDocumentoFederal();
    }

    public void selecionaPessoa() {
        documentoFederal = "";
        if (tipoPessoa.equals("PF")) {
            super.getEntidade().setPessoa(new ClienteFisica());
        } else {
            super.getEntidade().setPessoa(new ClienteJuridico());
        }
    }

    public void consultaDocumentoFederal() {
        try {
            if (tipoPessoa.equals("PF")) {
                ClienteFisica pf = pessoaFacade.buscaPorCpf(documentoFederal);
                super.getEntidade().setPessoa(pf);
            } else {
                ClienteJuridico pj = pessoaFacade.buscaPorCnpj(documentoFederal);
                super.getEntidade().setPessoa(pj);
            }
        } catch (Exception ex) {
            ControleUtil.mensagemSistema(FacesMessage.SEVERITY_INFO, "Cliente ainda não cadastrado", "");
            ex.printStackTrace();
        }
        super.getEntidade().getPessoa().setDocumentoFederal(documentoFederal);
    }

    public List<Cliente> clienteLikeNome(String query) {
        return clienteFacade.clienteLikeNome(query);
    }

    public ClienteControle() {
        super(Cliente.class);
    }

    @Override
    public AbstractFacade<Cliente> getFacade() {
        return clienteFacade;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getDocumentoFederal() {
        return documentoFederal;
    }

    public void setDocumentoFederal(String documentoFederal) {
        this.documentoFederal = documentoFederal;
    }

}

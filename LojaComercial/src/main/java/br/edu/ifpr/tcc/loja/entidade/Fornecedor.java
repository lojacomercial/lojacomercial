/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "fornecedor")
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name = "pessoa_id", referencedColumnName = "pessoa_id", foreignKey = @ForeignKey(name = "FK_FORNECEDOR_PESSOA"))
@AttributeOverrides({@AttributeOverride(name = "pessoa_id", column = @Column(name = "fornecedor_id"))})
public class Fornecedor extends Pessoa implements Serializable {

    @Column(name = "fornecedor_razaoSocial", nullable = false)
    private String razaoSocial;
    @Column(name = "fornecedor_nome_fantasia", nullable = false)
    private String nomeFantasia;
    @Column(name = "fornecedor_cnpj", nullable = false)
    private String cnpj;
    @Column(name = "fornecedor_dt_fundacao", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataFundacao;

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Date getDataFundacao() {
        return dataFundacao;
    }

    public void setDataFundacao(Date dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    @Override
    public String getDocumentoFederal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setDocumentoFederal(String doc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDocumentoEstadual() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}

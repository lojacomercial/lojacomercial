/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.seguranca;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rodolfo Cassorillo
 */
@WebFilter(filterName = "LoginFilter", urlPatterns = {"/app/*"})
public class LoginFilter implements Filter {

//    @Inject
//    private LoginControle loginControle;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

//        if (loginControle.getUsuario() != null) {
            chain.doFilter(request, response);
//        }else{
//            resp.sendRedirect(req.getContextPath()+"/login.xhtml");
//        }
    }

    @Override
    public void destroy() {

    }

}

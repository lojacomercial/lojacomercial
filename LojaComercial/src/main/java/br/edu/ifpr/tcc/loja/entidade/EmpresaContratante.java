/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

import java.io.Serializable;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Entity
@Audited
@Table(name = "empresa_contratante")
public class EmpresaContratante extends Fornecedor implements Serializable {

    @Column(name = "empresa_contratante_logo", nullable = false)
    private Blob logo;

    public Blob getLogo() {
        return logo;
    }

    public void setLogo(Blob logo) {
        this.logo = logo;
    }            
    
}

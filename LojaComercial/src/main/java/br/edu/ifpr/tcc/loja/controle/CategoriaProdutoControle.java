/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.controle;

import br.edu.ifpr.tcc.loja.entidade.CategoriaProduto;
import br.edu.ifpr.tcc.loja.facade.AbstractFacade;
import br.edu.ifpr.tcc.loja.facade.CategoriaProdutoFacade;
import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Rodolfo Cassorillo
 */
@Named
@ViewScoped
public class CategoriaProdutoControle extends AbstractControle<CategoriaProduto> 
                             implements Serializable{

    @Inject
    private CategoriaProdutoFacade categoriaProdutoFacade;
    
    public CategoriaProdutoControle() {
        super(CategoriaProduto.class);
    }

    @Override
    public AbstractFacade getFacade() {
        return categoriaProdutoFacade;
    }
    
    public List<CategoriaProduto> categoriaProdutoLikeDescricao(String query){
        return categoriaProdutoFacade.categoriaProdutoLikeDescricao(query);
    }
  
}

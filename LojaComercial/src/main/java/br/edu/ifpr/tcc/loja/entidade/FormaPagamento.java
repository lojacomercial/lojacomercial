/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpr.tcc.loja.entidade;

/**
 *
 * @author Rodolfo Cassorillo
 */
public enum FormaPagamento {

    DINHEIRO(1), 
    CHEQUE_A_VISTA(2), 
    CHEQUE_A_PRAZO(3), 
    CARTAO_A_VISTA(4), 
    CARTAO_A_PRAZO(5);

    public int formaPagamento;

    private FormaPagamento(int formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

}
